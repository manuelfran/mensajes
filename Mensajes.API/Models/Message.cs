﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mensajes.API.Models
{
    [Serializable]
    public class Message
    {
        public int Id { get; set; }
        public string msg { get; set; }
        public List<string> target { get; set; }
        public List<string> readers { get; set; }
    }
}
